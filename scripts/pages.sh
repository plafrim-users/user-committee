#!/bin/bash
mkdir -p public
cd web
touch index.org
emacs --batch --no-init-file --load ../scripts/publish.el --funcall org-publish-all
mv -f *.html ../public/
cp -r css/ ../public/
cp -r figures/ ../public/
cp -r doc/ ../public/

#sed -e 's/@@html:&lt;red&gt;@@/<red>/g' \
#    -e 's/@@html:&lt;\/red&gt;@@/<\/red>/g' \
#    -e 's/@@html:&lt;blue&gt;@@/<blue>/g' \
#    -e 's/@@html:&lt;\/blue&gt;@@/<\/blue>/g' \
#    -e 's/@@html:&lt;green&gt;@@/<green>/g' \
#    -e 's/@@html:&lt;\/green&gt;@@/<\/green>/g' \
#    -i ../public/*.html

