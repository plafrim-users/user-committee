* Réunion du 2 mai 2016
:PROPERTIES:
:CUSTOM_ID: 20160502
:END:

[[./doc/cumip-2016-05-02.pdf][présentation]]

** Présents
  - Bill Allombert (représentant d’Andreas Enge, Lfant)
  - Heloïse Beaugendre (Cardamom)
  - Michel Bergmann (Memphis)
  - Julien Diaz (Magic-3d)
  - Nathalie Furmento (Storm)
  - Robin Genuer (Sistm)
  - Brice Goglin (Tadaam)
  - Abdou Guermouche (Hiepacs)
  - Vincent Perrier (Cagire)
  - Francois Rué (Représentant comité technique)
  - Olivier Saut (Monc)
  - David Sherman (Pleiade)
  - Olivier Coulaud (Responsable PlaFRIM)

** Ordre du jour

*** Arrêt de PlaFRIM1

- Mise à jour de manumanu/minotaure

  Les équipes continuant à utiliser la machine n’ont pas besoin de
  l’accès au lustre, et aux modules partagés de la plateforme. Il
  semble toutefois primordial de mettre la machine à jour pour pouvoir
  avoir une version récente de la glibc (ou du moins équivalente à
  celle de PlaFRIM 2), et des outils SGI (notamment mpt).

  Le coût de la mise à jour par SGI étant élevée, la proposition est
  de laisser la machine dans son état actuel, et de tenter une
  installation CentOS avec un dualboot.

- Migration PlaFRIM1 vers PlaFRIM2

  La migration des machines va se faire au fur et à mesure. Du fait
  d’un manque d’ingénieurs et des problèmes de compatibilité IPMI et
  BIOS, la migration des machines est très lente.

  Il est demandé à ce que les mirabelles soient conservés comme noeuds
  de calcul et non comme machine de formation.

  CARDAMOM fait remonter des problèmes de transition de PlaFRIM1 vers
  PlaFRIM2 du fait de: modules de PlaFRIM1 non disponibles sur
  PlaFRIM2, baisse de performances entre PlaFRIM1 et PlaFRIM2

  MEMPHIS fait également remonter des problèmes dûs à des queues trop
  courtes, des erreurs “nodes failed”, des problèmes d’accès aux
  disque, des problèmes de documentation et d’information, et des
  problèmes d’utilisation de la file préemptive (aucune libération des
  ressources).

  ACTIONS

  1) Il est important que les utilisateurs fassent remonter la liste de
     modules manquants sur PlaFRIM2 pour qu’ils puissent être installés.
  2) Les problèmes de performances sont probablement liés à une
     configuration incomplète lors de la soumission (mauvaise ou non
     utilisation de variables d’environnement de SLURM). Il serait
     intéressant de mettre en place un accompagnement direct des
     utilisateurs sur l’utilisation de slurm.

  Autres points
  - Certains utilisateurs n’arrivant pas à se connecter directement
    sur PlaFRIM2, LFANT a demandé que soit mis en place une solution
    ne requiérant pas une configuration de ssh spécifique sur les
    postes des utilisateurs (ce qui est un obstacle pour certains).
    Une telle passerelle existe déjà pour PlaFRIM1. L’accès à la
    nouvelle plateforme étant régie par des règles très strictes de
    sécurité, ceci ne sera pas possible. Les utilisateurs ayant des
    soucis de connexion sont invités à se rapprocher de l’équipe
    technique.
  - LFANT suggère l’ajout à PlaFRIM2 de machines de développement pour
    un usage interactif, semblable aux devel2-9 de PlaFRIM1. L’idée
    étant de faire en sorte que PlaFRIM2 fournisse les même services
    que PlaFRIM1 (et plus bien sûr!).
  - Magic-3D demande s’il serait possible de faire passer certaines
    parties du site web contentant les informations de base sur le
    fonctionnement de privé à public ? Par exemple la liste des queues
    avec les limitations ?
    LFANT demande également que les informations sur les queues
    données sur le site soient considérées comme les informations
    officielles décidées par le comité des utilisateurs, et non les
    informations données par sinfo.

*** Animation scientifique

Une journée scientifique en commun avec le mésocentre est en cours
d’organisation. Plus de détails vous seront communiqués
ultérieurement. Merci de sensibiliser vos équipes à y participer.

Voici des suggestions de formation remontées par les utilisateurs:

- Formation VIHPS de la maison de la simulation donnée au niveau européen (scalasca, tau, maqao, paraver … ). On pourrait envisager de faire une session sur Bordeaux.
- Formation sur la visualisation en parallèle. Pas de formateur de ciblé, on pourrait demander à Martial Mancip (maison de la simulation).
- Formation sur l’usage des outils du MCIA ? Suite à une demande d’O. Coulaud, M. Bergmann indique qu’une doctorante de son équipe les utilisait mais elle est partie

*** Politique de mise à jour des systèmes, logiciels, bibliothèques, …

Pas de retour particulier des équipes.

En ce qui concerne les modules utilisateurs, cela pourrait être intéressant d’utiliser des outils de construction de modules comme easybuild, spack, ou ansible.

*** Discussion sur le système d’ordonnancement (queues de soumission, limitations)

LFANT voudrait allonger la limite de la queue longue sur souris à une semaine.

Pour la queue ~testpreempt~, il semblerait qu’il y ait un conflit de
préemption. En réservant moins de 24 noeuds sur cette queue, aucune
préemption n’est faite, en réservant plus de 24 noeuds, le job est
préempté. Il faudrait tester que la préemption fonctionne
correctement.

Il serait également intéressant d’avoir des statistiques d’utilisation
de la plateforme, en particulier, le temps d’attente moyen pour
l’exécution d’un job.

HiEPACS et STORM font remonter un problème pour la cohabitation de
jobs longs et de jobs courts. Une succession de jobs longs déjà
ordonnancée sur la machine retarde l’exécution de jobs courts, qu’on
aurait aimé pouvoir exécuter entre 2 jobs longs.

Les propositions retenues par le comité suite aux discussions sont:

- demande de mettre en place sur 16 machines mistral (Xeon Phi) une
  file longue d’une semaine. On garde 2 mistrals en dehors de cette
  file pour continuer à faire des travaux spécifiques sur les
  accélérateurs.
- demande de mettre en place sur les miriel une queue bloquée par
  défaut pour des travaux d’une durée de plus de 15 jours sur 8 à 10
  noeuds. L’utilisation de cette file sera faite par demande motivée à
  la commission utilisateurs, une absence de réponse sous 18h00
  équivaudra à une acceptation. Le responsable de la commission
  utilisateurs transmettra alors la demande au comité technique pour
  la mise en place de la file.
- Mettre en place une file de routage qui utiliserait une métrique
  d’utilisation CPUS (nb de coeurs * durée du job) pour permettre un
  meilleur partage et une meilleure utilisation des ressources pour
  les jobs longs et courts.

Toutes ces propositions seront étudiées par le comité technique et
transmises au bureau pour avis final.
